const numbers = [
  {amount: 2},
  {amount: 4},
  {amount: 34},
  {amount: 45},
  {amount: 65},
];

const sum = numbers.reduce((acc, {amount}) => {
  return acc + amount;
}, 0);

const sum = a = b => a + b;

sum(a)(b);
