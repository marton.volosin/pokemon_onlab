import React from 'react';
import {createDrawerNavigator} from '@react-navigation/drawer';
import {NavigationContainer} from '@react-navigation/native';
import {StackNavigator} from './StackNavigator';

const Drawer = createDrawerNavigator();

const RootNavigator = () => (
  <NavigationContainer>
    <Drawer.Navigator initialRouteName="StackNavigator">
      <Drawer.Screen name="StackNavigator" component={StackNavigator} />
    </Drawer.Navigator>
  </NavigationContainer>
);

export {RootNavigator};
