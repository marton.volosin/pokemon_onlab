import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import PokemonListScreen from '../screens/PokemonListScreen/PokemonListScreen';

const Stack = createStackNavigator();

const StackNavigator = () => (
  <Stack.Navigator>
    <Stack.Screen name="PokemonListScreen" component={PokemonListScreen} />
  </Stack.Navigator>
);

export {StackNavigator};
