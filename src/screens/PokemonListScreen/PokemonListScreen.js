import React, {useMemo, useState, useEffect} from 'react';
import {Text, FlatList, SafeAreaView, TextInput, View} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as actions from '../../redux/actions';
import {styles} from './styles';

const PokemonListScreen = props => {
  const {listPokemons, getPokemons, isLoading, error} = props;
  const [text, onChangeText] = useState('');

  useEffect(() => {
    getPokemons();
  }, [getPokemons]);

  const filteredList = useMemo(
    () =>
      listPokemons.filter(({name}) =>
        name.toUpperCase().includes(text.toUpperCase()),
      ),
    [listPokemons, text],
  );

  return (
    <SafeAreaView style={styles.container}>
      {isLoading || error ? (
        <Text>{error ? error : 'Loading ...'}</Text>
      ) : (
        <View style={styles.container}>
          <TextInput
            style={styles.inputText}
            onChangeText={output => onChangeText(output)}
            value={text}
          />
          <FlatList
            renderItem={({item}) => (
              <Text>
                {item.name + '            ' + item.types[0].type.name}
              </Text>
            )}
            data={filteredList}
            keyExtractor={item => item.name}
            style={[styles.container]}
          />
        </View>
      )}
    </SafeAreaView>
  );
};

const mapStateToProps = state => {
  return {
    listPokemons: state.pokemons.listPokemons,
    isLoading: state.pokemons.isFetching,
    error: state.pokemons.error,
  };
};

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getPokemons: actions.getPokemons,
    },
    dispatch,
  );

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(PokemonListScreen);
