import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  inputText: {
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
  },
});
