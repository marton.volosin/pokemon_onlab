const BASE_URL = 'https://pokeapi.co/api/v2';

module.exports = {
  BASE_URL,
};
