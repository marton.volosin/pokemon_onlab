// POKEMON FETCH TYPES

export const POKEMONS_FETCH_SUCCESS = 'pokemons_fetch_success';
export const POKEMONS_FETCH_START = 'pokemons_fetch_start';
export const POKEMONS_FETCH_FAILED = 'pokemons_fetch_failed';
