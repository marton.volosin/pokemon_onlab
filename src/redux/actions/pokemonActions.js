import {
  POKEMONS_FETCH_START,
  POKEMONS_FETCH_SUCCESS,
  POKEMONS_FETCH_FAILED,
} from '../types';

import {BASE_URL} from '../../resources/constants';

export const getPokemons = () => async (dispatch, getState) => {
  dispatch({
    type: POKEMONS_FETCH_START,
  });

  try {
    const pokemonListResponse = await fetch(
      `${BASE_URL}/pokemon?offset=40&limit=40`,
    );
    const pokemonList = await pokemonListResponse.json();
    const pokemonPromises = pokemonList.results.map(({name}) =>
      fetch(`${BASE_URL}/pokemon/${name}`),
    );
    const pokeDetailsResponses = await Promise.all(pokemonPromises);
    const pokemonDetailList = await Promise.all(
      pokeDetailsResponses.map(resp => resp.json()),
    );
    dispatch({
      type: POKEMONS_FETCH_SUCCESS,
      payload: pokemonDetailList,
    });
  } catch (e) {
    dispatch({
      type: POKEMONS_FETCH_FAILED,
      error: e.message,
    });
  }
};
