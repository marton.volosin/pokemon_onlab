import {
  POKEMONS_FETCH_START,
  POKEMONS_FETCH_SUCCESS,
  POKEMONS_FETCH_FAILED,
} from '../types';

const INITIAL_STATE = {
  listPokemons: [],
  isFetching: false,
  error: null,
};

export default (state = INITIAL_STATE, {type, payload, error}) => {
  switch (type) {
    case POKEMONS_FETCH_START:
      return {
        ...state,
        isFetching: true,
      };
    case POKEMONS_FETCH_SUCCESS:
      return {
        ...state,
        isFetching: false,
        listPokemons: payload,
      };
    case POKEMONS_FETCH_FAILED:
      return {
        ...state,
        isFetching: false,
        error,
      };
    default:
      return state;
  }
};
